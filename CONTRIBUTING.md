Notas de desenvolvimento
========================

Este documento descreve a configuração básica do ambiente de desenvolvimento.
Ele descreve como rodar o projeto com dados de exemplo e como rodar testes
após fazer modificações no código.

(for English version, see [contributing-en][contributing-en])


Pré-requisitos / instalação local
---------------------------------

Você pode executar a plataforma usando o Docker ou não. Usá-lo é útil para
manter o ambiente o mais padronizado e estável possível, já que a virtualização
isola os containers do resto do sistema operacional. Instalar os componentes
manualmente e direto na sua máquina pode ser mais simples se você não
conhece o suficiente de Docker, e é a forma preferida entre as pessoas que
trabalham no código do Radar :-).

### Sem docker

Você deve ter o Python2 instalado na máquina, e o instalador de pacotes pip.
Em sistemas GNU/Linux, ambos podem ser encontrados nos repositórios
(``python`` e ``python-pip``). Certifique-se de *estar utilizando python2*.

Verifique que a versão do python que você usa
é a 2, e não a 3 -- migração do código para funcionar completamente ainda
está em curso. Para ver a versão do seu python use o comando:

```
$ python --version
```

Recomendamos usar o virtualenv para facilitar o gerenciamento dos pacotes
python. Com o [virtualenv e o virtualenvwrapper instalados][guia-virtualenv],
basta criar o virtualenv:

```
$ mkvirtualenv -p /usr/bin/python2.7 radarlegislativo
```

Em seguida, instale os requerimentos do terminal estando dentro do diretório do
repositório (o mesmo deste arquivo) e com o virtualenv ativado:

```
$ pip install --upgrade -r requirements-dev.txt
```

É necessário também que o [elasticsearch 2.4][elasticsearch] esteja instalado.

Para sistemas Debian ou Ubuntu GNU/Linux, basta rodar o script incluído
nesse repositório:

```
$ ./scripts/get_elasticsearch.sh
```

Esse script fará o download do elasticsearch e o deixará disponível no lugar
esperado pelos outros scripts deste repositório.

Caso você não use uma dessas distribuições linux, siga as [instruções de
instalação][elastic-install].

É necessário também instalar o compilador [LESS][less-css] e o
rabbitmq. Nos sistemas Debian ou Ubuntu GNU/Linux, basta usar o
gerenciador de pacotes e npm:

```
$ sudo apt update
$ sudo apt -y upgrade
$ sudo apt install npm rabbitmq-server
$ sudo npm install -g less
```

Caso haja problemas na instalação do rabbitmq, isto pode ocorrer porque alguns sistemas já têm pré-instalado uma de suas versões, a qual provavelmente deve estar desatualizada. Basta então desinstalá-lo, rodar os scripts mantidos pelo projeto no Package Cloud e depois prosseguir com a instalação. 

```
$ sudo apt-get remove rabbitmq-server
$ curl -s https://packagecloud.io/install/repositories/rabbitmq/rabbitmq-server/script.deb.sh | sudo bash
$ sudo apt install npm rabbitmq-server
```

Para mais informações, acessar o site do projeto: https://www.rabbitmq.com/install-debian.html

### Com docker

Você vai precisar do [Docker][docker] e [Docker Compose][docker-compose]
instalados. Com isso execute:

```
$ docker-compose -f docker-compose.dev.yml build
```

Configuração e inicialização da plataforma localmente
-----------------------------------------------------

### Variávies de ambiente

Crie, no diretório raiz do projeto, um arquivo de configuração específico do
ambiente (você pode usar o exemplo inclúido como base):

```
$ cp example-env .env
```

Modifique o arquivo para que a linha que contém `DEBUG=False` fique
com `DEBUG=True`.

No arquivo `.env`, além de definir a variável `DEBUG` você pode
definir várias outras configurações. Por exemplo, o conteúdo de
`SECRET_KEY` deve ser único para cada instalação e mantido em segredo
por [questões de segurança][django_secret_key].

Para que o tramitabot funcione, você precisa definir neste mesmo
arquivo (`.env`) a variável `TRAMITABOT_API_TOKEN`, com o valor
fornecido no [processo de criação do seu bot][telegram_bot]. Se você não
precisa do tramitabot funcionando agora, você pode definir qualquer valor para
essa variável por enquanto.

Outras variáveis estão descritas na [documentação de deploy][deploy].

### Inicialização

#### Sem Docker

Crie o banco de dados e carregue os dados mínimos necessários:

```
$ make dev_init_db
```

e rode o ambiente de desenvolvimento

```
$ make dev
```

Em poucos segundos você já pode acessar a página no endereço
<http://localhost:8000>.

Na primeira execução (e somente na primeira) é necessário atualizar o índice do
elasticsearch. Para isso, basta executar (com o `make dev` rodando em outro
terminal):

```
$ python radarlegislativo/manage.py rebuild_index
```

Para ver as mudanças no site rapidamente, você pode fazer as alterações
enquanto mantém aberto um terminal com o ``make dev``.

Se você fez mudanças nos arquivos LESS que geram o CSS, você deve
compilar esses arquivos para que as mudanças tenham efeito:

```
$ make compile_css
```

Por favor, mantenha as alterações todas nos arquivos fonte (less)
originais. Todas as alterações feitas diretamente no arquivo
`radar.css` serão sobrescritas antes do deploy.

Se você pretende capturar dados reais do site da Câmara e do Senado, siga
adiante para a [configuração do processo de captura de projetos de
lei][captura].

#### Com Docker

Execute as migrações:

```
$ docker-compose -f docker-compose.dev.yml run --rm web python manage.py migrate
```

Depois importe os dados:

```
$ docker-compose -f docker-compose.dev.yml run --rm web python manage.py loaddata \
    main/fixtures/tags.json \
    main/fixtures/projetos.json \
    main/fixtures/tramitacoes.json \
    agenda/fixtures/comissoes.json
```

E crie os índices do Elasticsearch:

```
$ docker-compose -f docker-compose.dev.yml run --rm web python manage.py rebuild_index
```

Para iniciar a aplicação:

```
$ docker-compose -f docker-compose.dev.yml up
```

Administração da plataforma
---------------------------

Para acessar a interface administrativa do Radar Legislativo, onde se pode
adicionar e editar categorias, projetos de lei, eventos na Agenda e outras
informações, basta ir à página `/admin/` do servidor web. Rodando
localmente, esse endereço deve ser:

http://127.0.0.1:8000/admin/

Em um projeto recém instalado, o banco de dados de usuáries está vazio. Para
criar um login com acesso de administração, rode o seguinte comando:

```
$ python manage.py createsuperuser
```


Populando o banco de dados
--------------------------

Em [docs/captura.md][captura] é possível ver como pedir à plataforma para
baixar um projeto de lei pela linha de comando, e como configurar um arquivo
.yml para carregar uma lista de PLs.

Testes
------

Para manter a estabilidade do Radar Legislativo, temos uma série
de testes feitos para garantir que mudanças feitas no código não estão
interferindo com resultados esperados e característicos de um bom
funcionamento do software.

Contribuições no repositório estão sujeitas aos testes rodados na
infraestrutura de CI do Gitlab passarem 100% com elas aplicadas para serem
aceitas e incorporadas ao *master*. Eles estão definidos em arquivos
`tests.py` dentro de cada app django.

Para rodar os testes em sua própria máquina, sem precisar subir código abrir
um pipeline no Gitlab, basta executar:

```
$ python manage.py test
```

Lembre-se de estar com o virtualenv criado como explicado acima, e com a
versão 2 do python. Para aprender mais sobre testes sugerimos ler a [seção
sobre testes do Tutorial do Django][django-testes].

Raspador Legislativo
--------------------

Documentações específicas para integração com o [Raspador
Legislativo][raspador]. Para mais detalhes ver a página do projeto.

### Configuração de palavras chave para definir tema dos filtros

```
$ cp example-palavras_chave.json secrets/palavras_chave.json
```

O arquivo `palavras_chave.json` está em formato para humanos e deve seguir o exemplo.

Produção
--------

O site [Radar Legislativo][website]
ainda está em fase de testes. Caso encontre algum erro, por favor entre em
contato escrevendo para radar@codingrights.org ou abrindo uma issue no [respositório do projeto ](https://gitlab.com/codingrights/radarlegislativo/)

[guia-virtualenv]: https://medium.com/@otaviobn/ambiente-virtual-python-com-virtualenv-virtualenvwrapper-no-ubuntu-instala%C3%A7%C3%A3o-e-uso-5e6691b92695
[website]: https://radarlegislativo.org
[django_secret_key]: https://docs.djangoproject.com/en/1.11/ref/settings/#std:setting-SECRET_KEY
[telegram_bot]: https://core.telegram.org/bots#6-botfather
[deploy]: https://gitlab.com/codingrights/radarlegislativo/blob/master/docs/deploy.md
[captura]: https://gitlab.com/codingrights/radarlegislativo/blob/master/docs/captura.md
[elasticsearch]: https://www.elastic.co/guide/en/elasticsearch/reference/2.4/index.html
[elastic-install]: https://www.elastic.co/guide/en/elasticsearch/reference/2.4/_installation.html
[less-css]: http://lesscss.org/
[contributing-en]: https://gitlab.com/codingrights/pls/blob/master/CONTRIBUTING.en.md
[docker]: https://docs.docker.com/install/
[docker-compose]: https://docs.docker.com/compose/install/
[django-testes]: https://docs.djangoproject.com/pt-br/1.11/intro/tutorial05/
[raspador]: https://github.com/cuducos/raspadorlegislativo
