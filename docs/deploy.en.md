Website deploy at a production environment
==========================================

Pre-requisites
--------------

The deploy process uses docker containers to create an enrivonment easily
reproductible and stable. Because of this, it is necessary to [install
docoker][instalacao_docker] in your production server. All other
dependencies are installed automatically in each container.


In order to ease the deploy process, you may use the [included
`Makefile`][makefile]. In this case, it is necessary to install `make`. In
Debian GNU/Linux systems, you may just execute:


```
apt-get install make
```


Settings
--------

The production environment needs some configuration that are not exactly
necessary in the development environment. They must be defined in a `.env`
file at the root of the project (the same directory that contains
`Makefile`).


The variables that need to be defined are:

* `DEBUG` - in a production environment, it must be defined as `False`

* `SECRET_KEY` - must be a unique value for each installation and must be
  kept in secret for [security reasons][django_secret_key].

* `TRAMITABOT_API_TOKEN` - the API token provided by
  [@BotFather](https://telegram.me/BotFather) in the [bot creation process][telegram_bot].

* `POSTGRES_DB`, `POSTGRES_USER`, `POSTGRES_PASSWORD` - respectively, the
  dbname, username and password for the database to be used.

* `HOST_DATABASE_FILES` - the complete path of the directory in which the
  database files are located in the host.

* `HOST_ELASTICSEARCH_FILES` - the complete path of the directory in which
  the index files for full text search are located in the host.


Deploy
------

For deploying, you need just to be in the root directory for the project
(the one which contains the `Makefile`) and execute

```
make deploy
```

This will download all needed docker images, install the dependencies and
execute the services.


Update routines
---------------

To keep the project information, it is necessary to run regularly the
following command:

```
make update
```

In the same manner, in order for Tramitabot to send update messages, it is
necessary to execute regularly the following command:

```
make tramitabot_send_updates
```

A good way to schedule these regular updates is with cron (possibly using
users with less privilege, as described below).


Using cron with less privileged users
-------------------------------------

_This step is optional, it is just a way to configure regular updates. You
may jump over this step and make it the best way for you._

To execute the commands inside the docker containers, it is needed that the
user has privilege to execute the docker commands as a superuser. To execute
these commands in cron, it is needed that this privileges come without the
need to input a password.


A way to reduce the scope of this user's actions is to allow it to execute
only the needed actions as a superuser. For this, create the following
scripts:


`/usr/local/bin/update_projetos.sh`:
```
#!/bin/bash
cd <path_to_the_repository>
/usr/local/bin/docker-compose run --rm web python manage.py update_all_projetos
```

`/usr/local/bin/send_tramitabot_updates.sh`:
```
#!/bin/bash
cd <path_to_the_repository>
/usr/local/bin/docker-compose run --rm tramitabot python manage.py tramitabot_send_updates
```

Now, you need just to add the following to your `sudoers` file:

```
# User alias specification
User_Alias RADAR_USERS = radarlegislativo

# Cmnd alias specification
Cmnd_Alias UPDATE_PROJETOS = /usr/local/bin/update_projetos.sh, /usr/loca/bin/send_tramitabot_updates.sh

# User privilege specification
RADAR_USERS ALL=(root) NOPASSWD: UPDATE_PROJETOS
```

And schedule the cron updates:

```
00 05 * * * sudo /usr/local/bin/update_projetos.sh
00 10 * * 1-5 sudo /usr/local/bin/send_tramitabot_updates.sh
```



[instalacao_docker]: https://docs.docker.com/engine/installation/linux/docker-ce/debian/
[makefile]: https://gitlab.com/codingrights/radarlegislativo/blob/master/Makefile
[django_secret_key]: https://docs.djangoproject.com/en/1.11/ref/settings/#std:setting-SECRET_KEY
[telegram_bot]: https://core.telegram.org/bots#6-botfather
