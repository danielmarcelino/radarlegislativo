from functools import partial

from django.conf import settings
from django.utils.safestring import mark_safe

from website.models import WebsiteOptions


def _get_value(options, key):
    value = getattr(options, key)
    if not value:
        return value

    return mark_safe(value)


def custom_fields(_request):
    """This context processor injects in the template the custom settings set
    in a `website.models.WebsiteOptions` object. It also adds a variable to the
    context informing whether Tramitabot is set or not.
    """
    options = WebsiteOptions.objects.first()
    if not options:
        options = WebsiteOptions.objects.create()

    get_value = partial(_get_value, options)
    context = {
        field.name: get_value(field.name)
        for field in WebsiteOptions._meta.fields
        if field.name != 'id'
    }

    context['tramitabot'] = bool(settings.TRAMITABOT_API_TOKEN)
    return context
