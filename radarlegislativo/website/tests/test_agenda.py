# -*- coding: utf-8 -*-
# This file is part of Radar Legislativo
# Copyright © 2016 codingrights
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import datetime

from django.test import TestCase, RequestFactory, mock
from django.urls import reverse

from agenda.models import Evento
from website.views import ViewListagemSemanal


__all__ = ["TestAgendaDaSemanaView", "TestViewListagemSemanal"]


class TestAgendaDaSemanaView(TestCase):
    fixtures = ["tags", "comissoes", "eventos"]

    def test_agenda_retorna_200(self):
        response = self.client.get(reverse("website:agenda"))
        self.assertEqual(response.status_code, 200)

    def test_usa_o_template_certo(self):
        response = self.client.get(reverse("website:agenda"))
        self.assertTemplateUsed(response, "website/agenda.html")

    @mock.patch('website.views.Evento')
    def test_inclui_eventos_da_semana_selecionada(self, mocked_model):
        self.client.get(reverse("website:agenda"), {"data": "13/09/2017"})

        mocked_model.objects.na_mesma_semana_que_o_dia\
            .assert_called_once_with(datetime.date(2017, 9, 13))
        mocked_model.objects.all.assert_not_called()

    def test_filtra_por_origem_se_o_parametro_de_GET_for_fornecido(self):
        response = self.client.get(
            reverse("website:agenda"),
            {"data": "13/09/2017", "origem": "SE"})
        eventos_esperados = Evento.objects.\
            na_mesma_semana_que_o_dia(datetime.date(2017, 9, 13))\
            .filter(comissao__comissaosenado__isnull=False).order_by("data")
        self.assertEqual(list(eventos_esperados),
                         list(response.context["object_list"]))


class TestViewListagemSemanal(TestCase):
    def setUp(self):
        self.factory = RequestFactory()

    def _get_view(self, *args, **kwargs):
        view = ViewListagemSemanal()
        view.request = self.factory.get(reverse("website:agenda"),
                                        data=kwargs)
        return view

    def test_dias_da_semana(self):
        view = self._get_view(data="01/03/2018")
        dias_esperados = [
            datetime.date(2018, 2, 26),
            datetime.date(2018, 2, 27),
            datetime.date(2018, 2, 28),
            datetime.date(2018, 3, 1),
            datetime.date(2018, 3, 2),
            datetime.date(2018, 3, 3),
            datetime.date(2018, 3, 4),
        ]
        self.assertEqual(view.dias_da_semana, dias_esperados)

    def test_semanas_proximas(self):
        view = self._get_view(data="01/03/2018")
        semanas_esperadas = [
            (datetime.date(2018, 2, 5), datetime.date(2018, 2, 11)),
            (datetime.date(2018, 2, 12), datetime.date(2018, 2, 18)),
            (datetime.date(2018, 2, 19), datetime.date(2018, 2, 25)),
            (datetime.date(2018, 2, 26), datetime.date(2018, 3, 4)),
            (datetime.date(2018, 3, 5), datetime.date(2018, 3, 11)),
            (datetime.date(2018, 3, 12), datetime.date(2018, 3, 18)),
            (datetime.date(2018, 3, 19), datetime.date(2018, 3, 25)),
        ]
        self.assertEqual(view.semanas_proximas, semanas_esperadas)

    @mock.patch('website.views.ViewListagemSemanal.get_default_date')
    def test_usa_data_padrao_se_uma_nao_for_definida(self, mocked_method):
        mocked_method.return_value = datetime.date.today()
        view = self._get_view()
        used_date = view.date
        mocked_method.assert_called()
        self.assertEqual(used_date, datetime.date.today())

    def test_aceita_data_fornecida_como_parametro_de_GET(self):
        view = self._get_view(data="01/03/2018")
        self.assertEqual(view.date, datetime.date(2018, 3, 1))

    def test_filtra_por_origem_se_origem_esta_nos_parametros(self):
        view = self._get_view(data="01/03/2018", origem="CA")
        self.assertTrue(view.deve_filtrar_pela_origem())

    def test_nao_filtra_por_origem_se_origem_nao_esta_nos_parametros(self):
        view = self._get_view(data="01/03/2018")
        self.assertFalse(view.deve_filtrar_pela_origem())
