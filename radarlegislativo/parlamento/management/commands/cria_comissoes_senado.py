# -*- coding: utf-8 -*-
# This file is part of Radar Legislativo
# Copyright © 2016 codingrights
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import logging
from django.core.management.base import BaseCommand
from tapioca_senado import Senado

from parlamento.models import ComissaoSenado


logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = u"Popula banco com todas as comissões do Senado"

    def handle(self, *args, **options):
        senado = Senado()
        logger.info('Importando comissões do Senado')
        resposta = senado.comissoes().get()
        for comissao in resposta.ListaColegiados.Colegiados.Colegiado:
            nome = comissao.Nome._data
            if len(nome) > 255:
                nome = nome[:255]
            ComissaoSenado.objects.create(
                nome=nome,
                sigla=comissao.Sigla._data,
                id_api=comissao.Codigo._data,
                tipo=comissao.DescricaoTipoColegiado._data,
            )
