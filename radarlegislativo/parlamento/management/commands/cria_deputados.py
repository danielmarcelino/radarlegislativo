# -*- coding: utf-8 -*-
# This file is part of Radar Legislativo
# Copyright © 2016 codingrights
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from django.core.management.base import BaseCommand
from tapioca_camara import Camara

from parlamento.models import Deputado


class Command(BaseCommand):
    help = u"Popula banco com todos os deputados em exercício atualmente"

    def handle(self, *args, **options):
        camara = Camara()

        deputados_ids = []
        paginacao = {'itens': 100, 'pagina': 1}
        resposta = camara.deputados().get(params=paginacao)
        proxima_pagina = True
        while proxima_pagina:
            for deputado in resposta.dados:
                deputados_ids.append(deputado.id._data)
            paginacao['pagina'] += 1
            resposta = camara.deputados().get(params=paginacao)
            proxima_pagina = any(l for l in resposta.links if l.rel._data == 'next')

        deputados = []
        for dep_id in deputados_ids:
            deputado = camara.deputado(id=dep_id).get()
            dados = deputado.dados._data
            status = deputado.dados.ultimoStatus._data
            deputados.append(Deputado(
                id_api=dados['id'],
                nome=dados['nomeCivil'],
                nome_parlamentar=status['nome'],
                foto=status['urlFoto'],
                email=status['gabinete'].get('email'),
                partido_id=status['siglaPartido'],
                estado_id=status['siglaUf'],
                participacao=status['condicaoEleitoral'],
            ))

        Deputado.objects.bulk_create(deputados)
