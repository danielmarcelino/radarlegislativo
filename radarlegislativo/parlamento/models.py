# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.text import Truncator


@python_2_unicode_compatible
class Estado(models.Model):
    sigla = models.CharField('Sigla', max_length=2, primary_key=True)
    nome = models.CharField('Nome', max_length=100)

    def __str__(self):
        return self.nome


@python_2_unicode_compatible
class Partido(models.Model):
    sigla = models.CharField('Sigla', max_length=10, primary_key=True)
    nome = models.CharField('Nome', max_length=100)

    def __str__(self):
        return self.nome


@python_2_unicode_compatible
class Parlamentar(models.Model):
    id_api = models.IntegerField('ID na API')
    nome = models.CharField('Nome Completo', max_length=255)
    nome_parlamentar = models.CharField('Nome Parlamentar', max_length=255)
    partido = models.ForeignKey(
        Partido,
        verbose_name='Partido',
        related_name='%(class)s',
        on_delete=models.PROTECT
    )
    estado = models.ForeignKey(
        Estado,
        verbose_name='Estado',
        related_name='%(class)s',
        on_delete=models.PROTECT
    )
    participacao = models.CharField('Participação', max_length=20, null=True, blank=True)
    email = models.EmailField('E-mail', null=True, blank=True)
    pagina_oficial = models.URLField('Página Oficial')
    foto = models.URLField('Foto')
    facebook = models.URLField('Facebook', null=True, blank=True)
    twitter = models.URLField('Twitter', null=True, blank=True)

    def __str__(self):
        return self.nome

    class Meta:
        abstract = True
        verbose_name = 'Parlamentar'
        verbose_name_plural = 'Parlamentares'


@python_2_unicode_compatible
class Senador(Parlamentar):

    def __str__(self):
        return self.nome

    class Meta:
        verbose_name = 'Senador'
        verbose_name_plural = 'Senadores'


@python_2_unicode_compatible
class Deputado(Parlamentar):

    def __str__(self):
        return self.nome

    class Meta:
        verbose_name = 'Deputado'
        verbose_name_plural = 'Deputados'


@python_2_unicode_compatible
class Comissao(models.Model):
    id_api = models.IntegerField('ID na API da casa')
    nome = models.CharField('Nome', max_length=255)
    sigla = models.CharField('Sigla', max_length=255)
    link = models.URLField(blank=True)
    tipo = models.CharField('Tipo', max_length=255)

    class Meta:
        verbose_name = "Comissão"
        verbose_name_plural = "Comissões"

    def __str__(self):
        return '{} - {}'.format(Truncator(self.nome).chars(100), self.origem)

    @property
    def origem(self):
        try:
            camara = self.comissaocamara
        except Exception:
            camara = False
        return 'Câmara' if camara else 'Senado'


@python_2_unicode_compatible
class ComissaoSenado(Comissao):

    def __str__(self):
        return '{} - Senado'.format(self.nome)

    class Meta:
        verbose_name = 'Comissão do Senado'
        verbose_name_plural = 'Comissões do Senado'


@python_2_unicode_compatible
class ComissaoCamara(Comissao):

    def __str__(self):
        return '{} - Câmara'.format(self.nome)

    class Meta:
        verbose_name = 'Comissão da Câmara'
        verbose_name_plural = 'Comissões da Câmara'
