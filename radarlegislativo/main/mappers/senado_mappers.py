# coding: utf-8

import re
from datetime import datetime
from pytz import timezone


class SenadoProjectMapper:

    @classmethod
    def map(cls, data):
        data = data['DetalheMateria']['Materia']
        kwargs = {}

        temp = data['Autoria']['Autor']
        kwargs['autoria'] = '{} {}'.format(
            temp.get('SiglaTipoAutor', ''),
            temp.get('NomeAutor', ''),
        )

        temp = data['DadosBasicosMateria']
        kwargs['ementa'] = temp.get('EmentaMateria', '')
        apr = temp.get('DataApresentacao', '')
        kwargs['apresentacao'] = datetime.strptime(
            apr, '%Y-%m-%d'
        ).replace(tzinfo=timezone('America/Sao_Paulo'))

        temp = data['IdentificacaoMateria']
        kwargs['nome'] = '{} {}/{}'.format(
            temp.get('SiglaSubtipoMateria', ''),
            temp.get('NumeroMateria', '').lstrip('0'),
            str(kwargs['apresentacao'].year)
        )

        temp = data['SituacaoAtual']['Autuacoes']['Autuacao']
        kwargs['local'] = temp['Local']['NomeLocal']

        kwargs['apensadas'] = ApensadaMapper.map(data)

        return kwargs


class ApensadaMapper:

    @classmethod
    def map(cls, data):
        apensadas = []
        materias = data.get('MateriasAnexadas', {}).get('MateriaAnexada', None) or []

        for item in materias:
            temp = item['IdentificacaoMateria']
            apensadas.append('{} {}'.format(
                temp['SiglaSubtipoMateria'],
                temp['NumeroMateria'],
            ))

        return ', '.join(apensadas)


class TramitacaoMapper:

    @classmethod
    def map(cls, data):
        data = data['MovimentacaoMateria']['Materia']['Tramitacoes']['Tramitacao']
        for tramitacao in data:
            temp = tramitacao['IdentificacaoTramitacao']
            kwargs = {}

            kwargs['id_site'] = temp['CodigoTramitacao']
            kwargs['descricao'] = re.sub('\s\s*', ' ', temp['TextoTramitacao'].strip())
            if 'Situacao' in temp:
                kwargs['descricao'] += u' — ' + re.sub('\s\s*', ' ', temp['Situacao'].get('DescricaoSituacao', '').strip())

            apr = temp.get('DataTramitacao', '')
            kwargs['data'] = datetime.strptime(
                apr, '%Y-%m-%d'
            ).replace(tzinfo=timezone('America/Sao_Paulo'))

            local = temp['DestinoTramitacao']['Local']['NomeLocal']
            kwargs['local'] = re.sub('\s\s*', ' ', local.strip())[:255]

            yield kwargs
