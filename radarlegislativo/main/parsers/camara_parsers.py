import re
from bs4 import BeautifulSoup
from datetime import datetime
from hashlib import md5
from pytz import timezone

from utils.generics import decode


class LocalCamaraHTMLParser:

    @classmethod
    def parse(cls, html):
        html = BeautifulSoup(html, 'html.parser')
        table = html.find(attrs={"id": "pareceresValidos"})
        if not table:
            return ''

        table.findChild('td')
        text = table.get_text()
        text = re.sub('\s+', ' ', text).strip()
        return text


class TramitacaoCamaraHTMLParser:

    @classmethod
    def parse(cls, html):
        html = BeautifulSoup(html, 'html.parser')
        tramitacoes = html.find(attrs={"id": "tramitacoes"})

        tds = list(tramitacoes.find('table').find_all('td'))
        it = iter(tds)
        zipped = zip(it, it)[::-1]

        for date_element, text_element in zipped:

            data = DataTramitacaoElementParser.parse(date_element)
            local = LocalTramitacaoElementParser.parse(text_element)
            descricao = DescricaoTramitacaoElementParser.parse(text_element)
            tramitacao_id = data.strftime("%Y-%m-%d") + local + descricao
            tramitacao_id = md5(tramitacao_id.encode('utf-8')).hexdigest()

            yield {
                'data': data,
                'local': local,
                'descricao': descricao,
                'id_site': tramitacao_id,
            }


class DataTramitacaoElementParser:

    @classmethod
    def parse(cls, element):
        content = decode(element.text.strip())
        return datetime.strptime(
            content, '%d/%m/%Y'
        ).replace(tzinfo=timezone('America/Sao_Paulo'))


class LocalTramitacaoElementParser:

    @classmethod
    def parse(cls, element):
        content = decode(element.find(attrs={'class': 'paragrafoTabelaTramitacoes'}).text.strip())
        return re.sub('\s\s*', ' ', content)[:255]


class DescricaoTramitacaoElementParser:

    @classmethod
    def parse(cls, element):
        content = decode(element.find(attrs={'class': 'ulTabelaTramitacoes'}).text.strip())
        return re.sub('\s\s*', ' ', content)
