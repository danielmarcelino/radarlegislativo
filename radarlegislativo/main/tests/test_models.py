# -*- coding: utf-8 -*-
# This file is part of Radar Legislativo
# Copyright © 2016 codingrights
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import datetime

from django.core.exceptions import ValidationError
from django.test import TestCase

from model_mommy import mommy

from main.models import Documento, PalavraChave, Projeto, Tag, Tramitacao


__all__ = ["TesteTramitacao"]


class TesteTramitacao(TestCase):
    def test_salvar_tramitacao_muda_data_de_atualizacao_do_projeto(self):
        data_original = datetime.date(2015, 01, 01)
        proj = mommy.make(
            Projeto,
            ultima_atualizacao=data_original,
        )

        self.assertEqual(proj.ultima_atualizacao, data_original)

        data_da_tramitacao = datetime.date(2016, 05, 03)
        mommy.make(Tramitacao, projeto=proj, data=data_da_tramitacao)

        self.assertEqual(proj.ultima_atualizacao, data_da_tramitacao)

    def test_mostra_descricao_inteira_se_nao_termina_com_inteiro_teor(self):
        tramitacao = mommy.make(Tramitacao,
                                descricao="descricao")
        self.assertEqual(tramitacao.descricao_limpa, "descricao")

    def test_mostra_descricao_limpa_se_termina_com_inteiro_teor(self):
        tramitacao = mommy.make(Tramitacao,
                                descricao="descricao Inteiro teor")
        self.assertEqual(tramitacao.descricao_limpa, "descricao ")


class TestProjetosListandoDocumentos(TestCase):

    def setUp(self):
        self.projetos = mommy.make(Projeto, _quantity=3)
        self.tags = mommy.make(Tag, _quantity=3)
        self.palavras_chave = mommy.make(PalavraChave, _quantity=3)
        for proj, tag, palavra in zip(self.projetos, self.tags, self.palavras_chave):
            proj.tags.add(tag)
            proj.palavras_chave.add(palavra)

        self.documento1 = mommy.make(Documento, tags=[self.tags[0]])
        self.documento2 = mommy.make(Documento, projetos=[self.projetos[1]])
        self.documento3 = mommy.make(Documento, palavras_chave=[self.palavras_chave[2]])

    def test_documentos_por_tag(self):
        projeto = Projeto.objects.get(pk=self.projetos[0].pk)
        self.assertEqual(list(projeto.documentos), [self.documento1])

    def test_documentos_por_projeto(self):
        projeto = Projeto.objects.get(pk=self.projetos[1].pk)
        self.assertEqual(list(projeto.documentos), [self.documento2])

    def test_documentos_por_palavra_chave(self):
        projeto = Projeto.objects.get(pk=self.projetos[2].pk)
        self.assertEqual(list(projeto.documentos), [self.documento3])


class TestDocumentos(TestCase):
    def test_documentos_com_arquivo(self):
        mommy.make(Documento, arquivo=u'/srv/radarlegislativo/pip-selfcheck.json')
        self.assertEqual(1, Documento.objects.count())

    def test_documentos_com_url(self):
        mommy.make(Documento, url=u'www.google.com')
        self.assertEqual(1, Documento.objects.count())

    def test_documentos_sem_url_ou_arquivo(self):
        doc = Documento(
            titulo='teste',
        )
        with self.assertRaises(ValidationError):
            doc.clean()

    def test_documentos_com_arquivo_E_url(self):
        doc = Documento(
            titulo='teste',
            arquivo=u'/srv/radarlegislativo/pip-selfcheck.json',
            url=u'www.google.com'
        )
        with self.assertRaises(ValidationError):
            doc.clean()
