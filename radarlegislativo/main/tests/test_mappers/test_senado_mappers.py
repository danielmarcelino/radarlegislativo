# coding: utf-8
from datetime import date
from unittest import TestCase

from main.mappers import SenadoProjectMapper, TramitacaoMapper
from main.mappers.senado_mappers import ApensadaMapper


class ApensadaMapperTestCase(TestCase):

    def setUp(self):
        self.mapper = ApensadaMapper

    def test_maps_data_with_one_materia_anexada_correctly(self):
        data = {
            u'Autoria': {
                u'Autor': {
                    u'DescricaoTipoAutor': u'Senador',
                    u'NomeAutor': u'Antonio Carlos Valadares',
                    u'SiglaTipoAutor': u'SENADOR',
                },
            },
            u'IdentificacaoMateria': {
                u'NumeroMateria': u'00330',
                u'SiglaSubtipoMateria': u'PLS'
            },
            u'DadosBasicosMateria': {
                u'DataApresentacao': u'2013-08-13',
                u'EmentaMateria': u'Disp\xf5e sobre a prote\xe7\xe3o, o tratamento e o uso dos dados pessoais, e d\xe1 outras provid\xeancias.',
            },
            u'SituacaoAtual': {
                u'Autuacoes': {
                    u'Autuacao': {
                        u'Local': {
                            u'CodigoLocal': u'1998',
                            u'NomeLocal': u'Plen\xe1rio do Senado Federal',
                        }
                    }
                }
            },
            u'MateriasAnexadas': {
                u'MateriaAnexada': [{
                    u'IdentificacaoMateria': {
                        u'SiglaSubtipoMateria': u'PLS',
                        u'NumeroMateria': u'181',
                    }
                }]
            }
        }

        mapped = self.mapper.map(data)

        mapped = self.mapper.map(data)
        assert 'PLS 181' == mapped

    def test_maps_data_with_many_materia_anexada_correctly(self):
        data = {
            u'Autoria': {
                u'Autor': {
                    u'DescricaoTipoAutor': u'Senador',
                    u'NomeAutor': u'Antonio Carlos Valadares',
                    u'SiglaTipoAutor': u'SENADOR',
                },
            },
            u'IdentificacaoMateria': {
                u'NumeroMateria': u'00330',
                u'SiglaSubtipoMateria': u'PLS'
            },
            u'DadosBasicosMateria': {
                u'DataApresentacao': u'2013-08-13',
                u'EmentaMateria': u'Disp\xf5e sobre a prote\xe7\xe3o, o tratamento e o uso dos dados pessoais, e d\xe1 outras provid\xeancias.',
            },
            u'SituacaoAtual': {
                u'Autuacoes': {
                    u'Autuacao': {
                        u'Local': {
                            u'CodigoLocal': u'1998',
                            u'NomeLocal': u'Plen\xe1rio do Senado Federal',
                        }
                    }
                }
            },
            u'MateriasAnexadas': {
                u'MateriaAnexada': [{
                    u'IdentificacaoMateria': {
                        u'SiglaSubtipoMateria': u'PLS',
                        u'NumeroMateria': u'181',
                    }
                }, {
                    u'IdentificacaoMateria': {
                        u'SiglaSubtipoMateria': u'PLS',
                        u'NumeroMateria': u'131',
                    }
                }, {
                    u'IdentificacaoMateria': {
                        u'SiglaSubtipoMateria': u'RQJ',
                        u'NumeroMateria': u'45',
                    }
                }]
            }
        }

        mapped = self.mapper.map(data)
        assert 'PLS 181' in mapped
        assert 'PLS 131' in mapped
        assert 'RQJ 45' in mapped

    def test_maps_data_without_materia_anexada_correctly(self):
        data = {
            u'Autoria': {
                u'Autor': {
                    u'DescricaoTipoAutor': u'Senador',
                    u'NomeAutor': u'Antonio Carlos Valadares',
                    u'SiglaTipoAutor': u'SENADOR',
                },
            },
            u'IdentificacaoMateria': {
                u'NumeroMateria': u'00330',
                u'SiglaSubtipoMateria': u'PLS'
            },
            u'DadosBasicosMateria': {
                u'DataApresentacao': u'2013-08-13',
                u'EmentaMateria': u'Disp\xf5e sobre a prote\xe7\xe3o, o tratamento e o uso dos dados pessoais, e d\xe1 outras provid\xeancias.',
            },
            u'SituacaoAtual': {
                u'Autuacoes': {
                    u'Autuacao': {
                        u'Local': {
                            u'CodigoLocal': u'1998',
                            u'NomeLocal': u'Plen\xe1rio do Senado Federal',
                        }
                    }
                }
            },
        }

        mapped = self.mapper.map(data)
        assert '' == mapped


class SenadoProjectMapperTestCase(TestCase):

    def setUp(self):
        self.mapper = SenadoProjectMapper

    def test_maps_data_correctly(self):
        data = {
            u'DetalheMateria': {
                u'Materia': {
                    u'Autoria': {
                        u'Autor': {
                            u'DescricaoTipoAutor': u'Senador',
                            u'NomeAutor': u'Antonio Carlos Valadares',
                            u'SiglaTipoAutor': u'SENADOR',
                        },
                    },
                    u'IdentificacaoMateria': {
                        u'NumeroMateria': u'00330',
                        u'SiglaSubtipoMateria': u'PLS'
                    },
                    u'DadosBasicosMateria': {
                        u'DataApresentacao': u'2013-08-13',
                        u'EmentaMateria': u'Disp\xf5e sobre a prote\xe7\xe3o, o tratamento e o uso dos dados pessoais, e d\xe1 outras provid\xeancias.',
                    },
                    u'SituacaoAtual': {
                        u'Autuacoes': {
                            u'Autuacao': {
                                u'Local': {
                                    u'CodigoLocal': u'1998',
                                    u'NomeLocal': u'Plen\xe1rio do Senado Federal',
                                }
                            }
                        }
                    },
                    u'MateriasAnexadas': {
                        u'MateriaAnexada': [{
                            u'IdentificacaoMateria': {
                                u'SiglaSubtipoMateria': u'PLS',
                                u'NumeroMateria': u'181',
                            }
                        }]
                    }
                },
            }
        }

        mapped = self.mapper.map(data)

        assert mapped['nome'] == 'PLS 330/2013'
        assert mapped['autoria'] == 'SENADOR Antonio Carlos Valadares'
        assert mapped['ementa'] == u'Disp\xf5e sobre a prote\xe7\xe3o, o tratamento e o uso dos dados pessoais, e d\xe1 outras provid\xeancias.'
        assert mapped['apresentacao'].date() == date(2013, 8, 13)
        assert mapped['local'] == u'Plen\xe1rio do Senado Federal'
        assert 'PLS 181' in mapped['apensadas']

    def test_maps_without_apensadas_correctly(self):
        data = {
            u'DetalheMateria': {
                u'Materia': {
                    u'Autoria': {
                        u'Autor': {
                            u'DescricaoTipoAutor': u'Senador',
                            u'NomeAutor': u'Antonio Carlos Valadares',
                            u'SiglaTipoAutor': u'SENADOR',
                        },
                    },
                    u'IdentificacaoMateria': {
                        u'NumeroMateria': u'00330',
                        u'SiglaSubtipoMateria': u'PLS'
                    },
                    u'DadosBasicosMateria': {
                        u'DataApresentacao': u'2013-08-13',
                        u'EmentaMateria': u'Disp\xf5e sobre a prote\xe7\xe3o, o tratamento e o uso dos dados pessoais, e d\xe1 outras provid\xeancias.',
                    },
                    u'SituacaoAtual': {
                        u'Autuacoes': {
                            u'Autuacao': {
                                u'Local': {
                                    u'CodigoLocal': u'1998',
                                    u'NomeLocal': u'Plen\xe1rio do Senado Federal',
                                }
                            }
                        }
                    },
                },
            }
        }

        mapped = self.mapper.map(data)

        assert mapped['nome'] == 'PLS 330/2013'
        assert mapped['autoria'] == 'SENADOR Antonio Carlos Valadares'
        assert mapped['ementa'] == u'Disp\xf5e sobre a prote\xe7\xe3o, o tratamento e o uso dos dados pessoais, e d\xe1 outras provid\xeancias.'
        assert mapped['apresentacao'].date() == date(2013, 8, 13)
        assert mapped['local'] == u'Plen\xe1rio do Senado Federal'
        assert '' == mapped['apensadas']


class TramitacaoMapperTestCase(TestCase):

    def setUp(self):
        self.mapper = TramitacaoMapper

    def test_maps_data_correctly(self):
        data = {
            u'MovimentacaoMateria': {
                u'Materia': {
                    u'Tramitacoes': {
                        u'Tramitacao': [{
                            u'IdentificacaoTramitacao': {
                                u'CodigoTramitacao': u'2440888',
                                u'TextoTramitacao': u'Prejudicado em virtude da deliberação do PLC 53/2018, com o qual tramitava em conjunto.',
                                u'Situacao': {
                                    u'DescricaoSituacao': u'PREJUDICADA',
                                },
                                u'DataTramitacao': u'2018-07-10',
                                u'DestinoTramitacao': {
                                    u'Local': {
                                        u'NomeLocal': u'Coordena\xe7\xe3o de Arquivo',
                                    }
                                }
                            }}, {
                            u'IdentificacaoTramitacao': {
                                u'CodigoTramitacao': u'2440418',
                                u'TextoTramitacao': u'Encaminhado ao Plen\xe1rio.',
                                u'DataTramitacao': u'2018-07-05',
                                u'DestinoTramitacao': {
                                    u'Local': {
                                        u'NomeLocal': u'Secretaria de Atas e Di\xe1rios',
                                    }
                                }
                            }
                        }]
                    }
                }
            }
        }

        mapped = list(self.mapper.map(data))
        assert 2 == len(mapped)

        assert u'2440888' == mapped[0]['id_site']
        assert u'Prejudicado em virtude da deliberação do PLC 53/2018, com o qual tramitava em conjunto. — PREJUDICADA' == mapped[0]['descricao']
        assert date(2018, 7, 10) == mapped[0]['data'].date()
        assert u'Coordena\xe7\xe3o de Arquivo' == mapped[0]['local']

        assert u'2440418' == mapped[1]['id_site']
        assert u'Encaminhado ao Plen\xe1rio.' == mapped[1]['descricao']
        assert date(2018, 7, 5) == mapped[1]['data'].date()
        assert u'Secretaria de Atas e Di\xe1rios' == mapped[1]['local']
