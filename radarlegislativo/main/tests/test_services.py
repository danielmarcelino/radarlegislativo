from mock import patch
from unittest import TestCase

from main.services import fetch_camara_project, fetch_senado_project


class FetchCamaraProjectTestCase(TestCase):

    def setUp(self):
        patchers = [
            patch('main.services.APICamaraClient', spec=True),
            patch('main.services.WebCamaraClient', spec=True),
            patch('main.services.LocalCamaraHTMLParser', spec=True),
            patch('main.services.TramitacaoCamaraHTMLParser', spec=True),
            patch('main.services.CamaraProjectMapper', spec=True),
        ]
        self.m_api_client, self.m_web_client, self.m_local_parser, self.m_tram_parser, self.m_mapper = [p.start() for p in patchers]
        [self.addCleanup(p.stop) for p in patchers]

    def test_service_calls_clients_and_parsers_correctly(self):
        self.m_mapper.map.return_value = {'s': 1}
        data = fetch_camara_project(1)

        self.m_api_client().get.assert_called_once_with(1)
        self.m_web_client().get.assert_called_once_with(1)
        self.m_mapper.map.assert_called_once_with(
            self.m_api_client().get()
        )
        self.m_local_parser.parse.assert_called_once_with(
            self.m_web_client().get().content
        )
        self.m_tram_parser.parse.assert_called_once_with(
            self.m_web_client().get().content
        )
        assert 1 == data['s']
        assert self.m_web_client().get().content == data['html_original']
        assert self.m_local_parser.parse() == data['local']
        assert self.m_tram_parser.parse() == data['tramitacoes']


class FetchSenadoProjectTestCase(TestCase):

    def setUp(self):
        patchers = [
            patch('main.services.APISenadoClient', spec=True),
            patch('main.services.APISenadoMovimentacaoClient', spec=True),
            patch('main.services.TramitacaoMapper', spec=True),
            patch('main.services.SenadoProjectMapper', spec=True),
        ]
        self.m_client, self.m_mov_client, self.m_tram_mapper, self.m_project_mapper = [p.start() for p in patchers]
        [self.addCleanup(p.stop) for p in patchers]

    def test_service_calls_clients_and_parsers_correctly(self):
        self.m_project_mapper.map.return_value = {'s': 1}
        self.m_client().get.return_value = {'content': 2}

        data = fetch_senado_project(1)

        self.m_client().get.assert_called_once_with(1)
        self.m_mov_client().get.assert_called_once_with(1)
        self.m_project_mapper.map.assert_called_once_with(
            self.m_client().get()
        )
        self.m_tram_mapper.map.assert_called_once_with(
            self.m_mov_client().get()
        )
        assert 1 == data['s']
        assert 2 == data['html_original']
        assert self.m_tram_mapper.map() == data['tramitacoes']
