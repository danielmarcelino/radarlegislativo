# encoding: utf-8
from datetime import date
from unittest import TestCase

from main.parsers import LocalCamaraHTMLParser, TramitacaoCamaraHTMLParser


class LocalCamaraHTMLParserTestCase(TestCase):

    def setUp(self):
        self.parser = LocalCamaraHTMLParser

    def test_parses_data_correctly(self):
        fixture = 'main/tests/test_parsers/fixtures/camara_response.html'
        with open(fixture, 'rb') as response:
            data = response.read()

        expected = u'Comissão Parecer Comissão de Desenvolvimento Econômico, Indústria, Comércio e Serviço   ( CDEICS ) 12/12/2016 - Parecer do Relator, Dep. Vinicius Carvalho (PRB-SP), pela rejeição deste, e do PL 6667/2009, apensado. Inteiro teor 05/04/2017   12:30 Reunião Deliberativa Ordinária Aprovado o Parecer. Comissão de Ciência e Tecnologia, Comunicação e Informática   ( CCTCI ) 13/06/2017 - Parecer do Relator, Dep. Domingos Neto (PSD-CE), pela rejeição deste, e do PL 6667/2009, apensado. Inteiro teor Comissão de Constituição e Justiça e de Cidadania   ( CCJC ) -'
        parsed = self.parser.parse(data)

        assert expected == parsed


class TramitacaoCamaraHTMLParserTestCase(TestCase):

    def setUp(self):
        self.parser = TramitacaoCamaraHTMLParser

    def test_parses_data_correctly(self):
        fixture = 'main/tests/test_parsers/fixtures/camara_response.html'
        with open(fixture, 'rb') as response:
            data = response.read()

        parsed = list(self.parser.parse(data))
        assert 69 == len(parsed)
        assert u'PLENÁRIO ( PLEN )' == parsed[-1]['local']
        assert date(2004, 8, 11) == parsed[-1]['data'].date()
        assert 'America/Sao_Paulo' == str(parsed[-1]['data'].tzinfo)
        assert u'Apresentação do Projeto de Lei pelo Deputado Cláudio Magrão (PPS-SP). Inteiro teor' == parsed[-1]['descricao']
        assert u'4eec7c8705b7d9436289449a717aff92' == parsed[-1]['id_site']
