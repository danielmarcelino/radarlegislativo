#-*- coding: utf-8 -*-
# This file is part of Radar Legislativo
# Copyright © 2016 codingrights
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import logging
from django.core.management.base import BaseCommand
from unidecode import unidecode

from main.models import Projeto, Tag
from main.tools.use_cases import DownloadProjects, DownloadProjectsFromFile


TAGS = Tag.objects.all()
tags_list = [u"{} - {}".format(t.slug, t.nome) for t in TAGS]
tags_help_text = (
    u"Tags a serem adicionadas ao projeto (escolha entre as seguintes: {})"
).format(", ".join(tags_list))


logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class Command(BaseCommand):
    help = u"Recupera informações de projetos da Câmara ou do Senado"

    def add_arguments(self, parser):
        parser.add_argument(
            "--id", dest="id_site", type=int,
            help=u"Id do projeto no site do Senado ou da Câmara"
        )
        parser.add_argument(
            "--origem", dest="origem", type=str, choices=("camara", "senado"),
            help=u"A origem do projeto"
        )
        parser.add_argument(
            "--tags", dest="tags", nargs="+", help=tags_help_text
        )
        parser.add_argument(
            "--from-file", dest="filename",
            help="Usa ids e tags e origem de um arquivo yaml"
        )

    def handle(self, *args, **options):

        if options.get('filename'):
            uc = DownloadProjectsFromFile()
            uc.execute(options['filename'])
        else:
            if not options.get("origem", False):
                self.stderr.write(
                    u"Para buscar dados de um projeto é necessário informar a origem."
                )

            SOURCES = {
                unidecode(v.lower()): k for k, v in Projeto.ORIGEM_CHOICES
            }
            source = SOURCES[options.get('origem')]
            external_id = options.get('id_site')

            uc = DownloadProjects()
            tag_ids = list(TAGS.values_list('id', flat=True))
            uc.execute(
                source=source,
                external_id=external_id,
                tags=tag_ids
            )
