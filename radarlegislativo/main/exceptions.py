class UnexpectedProjectSourceError(Exception):
    '''
        Exception to be raised when an unexpected value of project source
        is used. See the expected values in main.models.Projetos.ORIGEM_CHOICES.
    '''


class NoneProjectSourceError(Exception):
    '''
        Exception to be raised when the project source is None.
    '''
