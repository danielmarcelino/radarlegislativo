from camara.clients import APICamaraClient, WebCamaraClient
from main.parsers import LocalCamaraHTMLParser, TramitacaoCamaraHTMLParser
from main.mappers import CamaraProjectMapper, SenadoProjectMapper, TramitacaoMapper
from senado.clients import APISenadoClient, APISenadoMovimentacaoClient


def fetch_camara_project(camara_id):

    client = APICamaraClient()
    data = client.get(camara_id)
    kwargs = CamaraProjectMapper.map(data)

    client = WebCamaraClient()
    response = client.get(camara_id)
    html = response.content

    kwargs['html_original'] = html
    kwargs['local'] = LocalCamaraHTMLParser.parse(html)
    kwargs['tramitacoes'] = TramitacaoCamaraHTMLParser.parse(html)

    return kwargs


def fetch_senado_project(senado_id):

    client = APISenadoClient()
    data = client.get(senado_id)
    kwargs = SenadoProjectMapper.map(data)
    kwargs['html_original'] = data['content']

    client = APISenadoMovimentacaoClient()
    data = client.get(senado_id)
    kwargs['tramitacoes'] = TramitacaoMapper.map(data)

    return kwargs
