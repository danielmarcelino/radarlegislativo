# -*- coding: utf-8 -*-
# This file is part of Radar Legislativo
# Copyright © 2017 codingrights
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import textwrap
import time
import sys
from django.conf import settings
from django.db import IntegrityError
from django.template.loader import render_to_string
import telepot

from main.models import Tramitacao
from .models import TramitabotUser

HELP_TEXT = (
u"""
Esse bot envia notificações de atualizações nos projetos de lei que são relevantes para a privacidade, liberdade de expressão, acesso e questões de gênero no meio digital. A lista  completa dos projetos atualmente monitorados está em https://codingrights.gitlab.io/pls/.

Esta é a versão beta, logo teremos mais funcionalidades e comandos implementados. Se você tem sugestões, também pode entrar em contato pelo email contact@codingrights.org

Use os seguintes comandos para interagir com o bot:

/cadastrar - Para passar a receber notificações.
/descadastrar - Para deixar de receber notificações.
/ajuda - Para mostrar esse texto.
/ultimas - Para mostrar últimas tramitações (você pode, se quiser, informar o número de tramitações que quer ver. Por exemplo: /ultimas 5).


""")


class Tramitabot(telepot.Bot):

    handlers = {
        "/start": "print_help",
        "/ajuda": "print_help",
        "/help": "print_help",
        "/ultimas": "ultimas",
        "/últimas": "ultimas",
        "/cadastrar": "register_user",
        "/descadastrar": "unregister_user",
    }

    help_text = HELP_TEXT
    registered_message = (u"A partir de agora você vai receber notificações "
                          u"regulares com as últimas tramitações.")
    user_already_registered_message = (u"Esta conta já está cadastrada para "
                                       u"receber notificações com as últimas "
                                       u"tramitações.")
    default = "default_handler"
    tramitacao_template = "tramitabot/tramitacao.txt"
    tramitacao_header_template = "tramitabot/header_tramitacoes.txt"
    tramitacao_footer_template = "tramitabot/footer_tramitacoes.txt"
    chars_limit = 4096

    def __init__(self):
        return super(Tramitabot, self).__init__(settings.TRAMITABOT_API_TOKEN)

    def print_help(self, msg):
        return self.help_text

    def default_handler(self, msg):
        return textwrap.dedent(u"""
        Não entendi...
        /ajuda mostra todos os comandos válidos :)
        """)

    def register_user(self, msg):
        user_info = {
            "username": msg["from"].get("username", ""),
            "first_name": msg["from"].get("first_name", ""),
            "last_name": msg["from"].get("last_name", ""),
            "telegram_id": msg["from"]["id"],
            "language_code": msg["from"].get("language_code", ""),
        }

        try:
            user, created = TramitabotUser.objects.get_or_create(**user_info)
        except IntegrityError:
            return self.user_already_registered_message

        return self.registered_message

    def unregister_user(self, msg):
        telegram_id = msg["from"]["id"]
        try:
            user = TramitabotUser.objects.get(telegram_id=telegram_id)
            user.delete()
        except TramitabotUser.DoesNotExist:
            pass
        return (u"A partir de agora você não vai mais receber "
                u"notificações automáticas desse bot.")

    def get_ultimas_tramitacoes(self, n):
        header = render_to_string(self.tramitacao_header_template)
        blocks = [header]
        for tramitacao in Tramitacao.objects.order_by("-data")[:n]:
            blocks.append(render_to_string(
                self.tramitacao_template, {'tramitacao': tramitacao}))
        footer = render_to_string(self.tramitacao_footer_template)
        blocks.append(footer)
        return blocks

    def ultimas(self, msg):
        text = msg["text"]
        splitted_text = text.split()
        if len(splitted_text) == 1:
            n = 15
        else:
            try:
                n = int(splitted_text[1].strip())
            except ValueError:
                return textwrap.dedent(u"""
                Esse comando deve ser usado de uma das seguintes formas:
                /ultimas
                /ultimas _n_ (onde _n_ é um número entre 1 e 20)
                """)
            if n > 20 or n <= 0:
                return u"O número de tramitações deve ser entre 1 e 20."
        return self.get_ultimas_tramitacoes(n)

    def prepare_message_by_blocks(self, blocks):
        next_msg = ""
        for block in blocks:
            if len(block) > self.chars_limit:
                raise ValueError(
                    u"Tramitabot can't send a block with more "
                    u"than {} characters.".format(self.chars_limit)
                )

            if len(next_msg) + len(block) > self.chars_limit:
                yield next_msg
                next_msg = ""

            next_msg += block
        else:
            yield next_msg

    def prepare_message(self, msg):
        limit = self.chars_limit
        return [msg[i:i + limit] for i in range(0, len(msg), limit)]

    def send_message(self, telegram_id, msg, parse_mode="Markdown",
                     disable_web_page_preview=True):
        if isinstance(msg, list):
            message = self.prepare_message_by_blocks(msg)
        else:
            message = self.prepare_message(msg)

        for msg_part in message:
            try:
                self.sendMessage(
                    telegram_id,
                    msg_part,
                    parse_mode=parse_mode,
                    disable_web_page_preview=disable_web_page_preview,
                )
            except telepot.exception.BotWasBlockedError:
                sys.stderr.write("User with id {} has blocked "
                                 "tramitabot\n".format(telegram_id))
                TramitabotUser.objects.mark_as_blocked(telegram_id)
            except telepot.exception.TelegramError as exc:
                sys.stderr.write("There was an error while sending updates "
                                 "to user {}: {}\n".format(telegram_id, exc))
            time.sleep(1)

    def handle_messages(self, msg):
        content_type, chat_type, chat_id = telepot.glance(msg)
        if content_type == "text":
            text = msg["text"]
            command = text.split()[0]

            handler = getattr(self, self.handlers.get(command,
                self.default))
            user_id = msg["from"]["id"]

            self.send_message(user_id, handler(msg))

    def send_updates(self):
        msg = self.get_ultimas_tramitacoes(15)
        for user in TramitabotUser.objects.filter(has_blocked_us=False):
            self.send_message(user.telegram_id, msg)

    def listen(self):
        return self.message_loop(self.handle_messages,
                run_forever=u"Tramitabot esta esperando mensagens...")
