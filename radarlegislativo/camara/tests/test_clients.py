from mock import Mock, patch
from unittest import TestCase

from camara.clients import APICamaraClient, BaseClient, WebCamaraClient
from camara.exceptions import ClientConnectionError


class APICamaraClientTestCase(TestCase):

    def setUp(self):
        self.client = APICamaraClient()
        patchers = [
            patch('camara.clients.suds', spec=True),
            patch('camara.clients.serializers', spec=True),
        ]
        self.m_client, self.m_serializer = [p.start() for p in patchers]
        [self.addCleanup(p.stop) for p in patchers]

    def test_class_has_correct_setup(self):
        expected_url = 'http://www.camara.leg.br/SitCamaraWS/Proposicoes.asmx?wsdl'
        assert expected_url == self.client.get_absolute_url()
        assert isinstance(self.client, BaseClient)

    def test_get_returns_data_correctly(self):
        expected_url = 'http://www.camara.leg.br/SitCamaraWS/Proposicoes.asmx?wsdl'
        self.m_client.client.Client().service.ObterProposicaoPorID.return_value = {'a': 1}
        self.m_serializer.serialize.return_value = {'b': 2}

        data = self.client.get(1)

        self.m_client.client.Client.assert_called_with(expected_url)
        self.m_client.client.Client().service.ObterProposicaoPorID.assert_called_once_with(1)
        self.m_serializer.serialize.assert_called_once_with({'a': 1})
        assert {'b': 2} == data

    def test_get_raises_exception_when_response_contains_error(self):
        self.m_client.client.Client().service.ObterProposicaoPorID.return_value = {'erro': 1}

        self.assertRaises(ClientConnectionError, self.client.get, 1)


class WebCamaraClientTestCase(TestCase):

    def setUp(self):
        self.client = WebCamaraClient()
        patcher = patch('camara.clients.requests', spec=True)
        self.m_requests = patcher.start()
        self.m_requests.codes = Mock(OK=200)
        self.addCleanup(patcher.stop)

    def test_class_has_correct_setup(self):
        expected_url = 'http://www.camara.leg.br/proposicoesWeb/fichadetramitacao'
        assert expected_url == self.client.get_absolute_url()
        assert isinstance(self.client, BaseClient)

    def test_get_returns_correct_data(self):
        expected_response = Mock(status_code=200)
        self.m_requests.get.return_value = expected_response
        expected_url = 'http://www.camara.leg.br/proposicoesWeb/fichadetramitacao'
        expected_params = {'idProposicao': 1}

        data = self.client.get(1)

        self.m_requests.get.assert_called_once_with(expected_url, expected_params)
        assert expected_response == data

    def test_get_raises_exception_when_response_isnt_okay(self):
        pass
