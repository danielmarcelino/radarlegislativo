# -*- coding: utf-8 -*-
# This file is part of Radar Legislativo
# Copyright © 2016 codingrights
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.core.exceptions import ValidationError
from django.test import TestCase
from model_mommy import mommy

from api.models import UltimaNotificacao


class TestUltimaNotificacao(TestCase):

    def setUp(self):
        self.emails = ('um@dois.tr', 'es@quatro.cinco')
        self.projeto = mommy.make(UltimaNotificacao, destinatarios=self.emails)

    def test_unico_registro(self):
        with self.assertRaises(ValidationError):
            UltimaNotificacao.objects.create(
                assunto='403',
                destinatarios='nope@nope.nope',
                corpo='Vazio'
            )
        self.assertEqual(1, UltimaNotificacao.objects.count())

    def test_edicao(self):
        self.projeto.corpo = 'editou!'
        self.projeto.save()
        self.assertEqual('editou!', UltimaNotificacao.objects.first().corpo)

    def test_campo_destinatarios(self):
        self.projeto.destinatarios = ', '.join(self.emails)
