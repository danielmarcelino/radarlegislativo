# -*- coding: utf-8 -*-
# This file is part of Radar Legislativo
# Copyright © 2016 codingrights
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.core import mail
from django.test import TestCase, override_settings
from freezegun import freeze_time
from model_mommy import mommy

from api.models import UltimaNotificacao
from api.tasks import notificacao_raspador
from main.models import Projeto


class TestNotificacaoRaspador(TestCase):

    @freeze_time('2018-07-01')
    def setUp(self):
        self.projeto = mommy.make(Projeto, publicado=False)

    @freeze_time('2018-07-10')
    def test_nada_a_notificar(self):
        self.ultima_notificacao = mommy.make(
            UltimaNotificacao,
            enviada_em='2018-07-05 00:00:00'
        )
        notificacao_raspador()
        self.assertFalse(mail.outbox)

    @freeze_time('2018-07-10')
    @override_settings(DEFAULT_FROM_EMAIL='no-reply@radar.org')
    @override_settings(NOTIFICATION_EMAILS=('um@radar.org', 'dois@radar.org'))
    @override_settings(HOST_FOR_EMAIL_LINKS='http://testserver')
    def test_primeira_notificacao(self):
        notificacao_raspador()
        self._assert_email_as_expected()
        self.assertEqual(1, UltimaNotificacao.objects.count())

    @override_settings(DEFAULT_FROM_EMAIL='no-reply@radar.org')
    @override_settings(NOTIFICATION_EMAILS=('um@radar.org', 'dois@radar.org'))
    @override_settings(HOST_FOR_EMAIL_LINKS='http://testserver')
    def test_notificacao(self):
        with freeze_time('2018-07-01'):
            self.ultima_notificacao = mommy.make(UltimaNotificacao)

        with freeze_time('2018-07-10'):
            notificacao_raspador()

        self._assert_email_as_expected()
        self.assertEqual(1, UltimaNotificacao.objects.count())

    def _assert_email_as_expected(self):
        self.assertTrue(mail.outbox)
        email = mail.outbox[0]

        self.assertEqual('no-reply@radar.org', email.from_email)
        self.assertEqual(['um@radar.org', 'dois@radar.org'], email.to)
        self.assertEqual(
            u'Novos PLs encontrados pelo Raspador Legislativo',
            email.subject
        )

        contents = (
            'http://testserver{}'.format(self.projeto.link_para_edicao()),
            self.projeto.nome,
            self.projeto.autoria,
            self.projeto.ementa,
            self.projeto.tag_list(),
            self.projeto.link
        )

        for content in contents:
            self.assertIn(content, email.body)
