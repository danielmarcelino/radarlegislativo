# -*- coding: utf-8 -*-
# This file is part of Radar Legislativo
# Copyright © 2016 codingrights
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json

from django.test import TestCase, override_settings
from django.urls import reverse
from mock import patch
from model_mommy import mommy


from main.models import Projeto, Tag
from main.use_cases import FetchProjectsFromCamara


class TestRaspadorNovoProjeto(TestCase):

    def setUp(self):
        self.data = {
            "id_site": u"31415",
            "origem": u"CA",
            "palavras_chave": u"teste",
            "token": u"chave"
        }
        self.url = reverse("api:projeto-novo")
        self.tag_teste = mommy.make(Tag, nome=u"Tag teste")

    def test_novo_projeto_so_aceita_post(self):
        response = self.client.get(self.url)
        self.assertEqual(405, response.status_code)

    @override_settings(RASPADOR_API_TOKEN=u"chave")
    @override_settings(PALAVRAS_CHAVE={u"teste": u"Tag teste"})
    @patch.object(FetchProjectsFromCamara, 'execute')
    def test_novo_projeto(self, mock_execute):
        response = self.client.post(self.url, data=self.data)
        self.assertEqual(201, response.status_code)
        mock_execute.assert_called_once_with(
            external_id=31415,
            tag_ids=(self.tag_teste.id,),
            keywords_ids=(1,),
            publish=False,
        )

    @override_settings(RASPADOR_API_TOKEN="chave")
    def test_projeto_invalido(self):
        data = self.data.copy()
        data["origem"] = "XX"
        response = self.client.post(self.url, data=data)
        self.assertFalse(Projeto.publicados.exists())

        data = json.loads(response.content.decode('utf-8'))
        self.assertEqual(400, response.status_code)
        self.assertIn("origem", data)

    @override_settings(RASPADOR_API_TOKEN="foobar")
    def test_token_invalido(self):
        response = self.client.post(self.url, data=self.data)
        self.assertFalse(Projeto.publicados.exists())

        data = json.loads(response.content.decode('utf-8'))
        self.assertEqual(400, response.status_code)
        self.assertIn("token", data)
