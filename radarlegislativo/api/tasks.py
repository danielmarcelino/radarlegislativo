#-*- coding: utf-8 -*-
# This file is part of Radar Legislativo
# Copyright © 2016 codingrights
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from celery import shared_task
from django.conf import settings
from django.core import mail
from django.template.loader import render_to_string

from api.models import UltimaNotificacao
from main.models import Projeto


@shared_task
def notificacao_raspador():
    """Envia notificação por email para avisar de novos projetos incluídos"""
    if not all((settings.NOTIFICATION_EMAILS, settings.DEFAULT_FROM_EMAIL)):
        return

    data_ultima_notificacao = None
    ultima_notificacao = UltimaNotificacao.objects.last()

    if ultima_notificacao:
        data_ultima_notificacao = ultima_notificacao.enviada_em

    novos = Projeto.nao_publicados.all()
    if data_ultima_notificacao:
        novos = novos.filter(cadastro__gte=data_ultima_notificacao)

    if not novos.exists():
        return

    assunto = u'Novos PLs encontrados pelo Raspador Legislativo'
    corpo = render_to_string(
        'api/notificacao_raspador.txt',
        dict(novos=novos, host=settings.HOST_FOR_EMAIL_LINKS)
    )
    mail.send_mail(
        assunto,
        corpo,
        settings.DEFAULT_FROM_EMAIL,
        settings.NOTIFICATION_EMAILS
    )

    notificacao = {
        'assunto': assunto,
        'destinatarios': settings.NOTIFICATION_EMAILS,
        'corpo': corpo
    }
    if not ultima_notificacao:
        UltimaNotificacao.objects.create(**notificacao)
    else:
        UltimaNotificacao.objects.update(**notificacao)
