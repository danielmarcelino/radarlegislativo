# -*- coding: utf-8 -*-

from django import template

register = template.Library()



@register.filter(takes_context=True)
def absolute_url_for(path, request):
    return request.build_absolute_uri(path)
