import requests

from senado import serializers
from senado.exceptions import ClientConnectionError


class BaseClient:

    BASE_PATH = u'http://legis.senado.leg.br/dadosabertos'
    PATH = ''

    def get_absolute_url(self):
        return '{}/{}'.format(self.BASE_PATH, self.PATH)


class APISenadoClient(BaseClient):

    PATH = u'materia'

    def get(self, project_id):
        url = '{}/{}'.format(self.get_absolute_url(), project_id)
        response = requests.get(url)

        if not response.status_code == requests.codes.OK:
            raise ClientConnectionError(u'An error was occoured while trying to get project id: {}'.format(project_id))

        kwargs = serializers.serialize(response.content)
        kwargs['content'] = response.content
        return kwargs


class APISenadoMovimentacaoClient(BaseClient):

    PATH = u'materia/movimentacoes'

    def get(self, project_id):
        url = '{}/{}'.format(self.get_absolute_url(), project_id)
        response = requests.get(url)

        if not response.status_code == requests.codes.OK:
            raise ClientConnectionError(u'An error was occoured while trying to get project id: {}'.format(project_id))

        return serializers.serialize(response.content)
